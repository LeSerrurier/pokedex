import React from 'react';
import { StyleSheet, View } from 'react-native';
import { AuthContextProvider } from './src/context/AuthContext';
import MainRouter from './src/router/MainRouter';

export default function App() {
  return (
    <View style={styles.container}>
      <AuthContextProvider >
        <MainRouter />
      </AuthContextProvider>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
