import firestore from '@react-native-firebase/firestore';
import {firebase} from '@react-native-firebase/auth';
import {PokemonLikedType} from "../type/type"

const idPokemonLiked = () => 
	firestore()
	.collection("users")
	.doc(firebase.auth().currentUser?.uid)
	.collection("pokemonLiked")

export const getIdPokemon: () => Promise<PokemonLikedType[]> = async () => {
	const querySnapshot = await idPokemonLiked().get()
	const docs = querySnapshot.docs

	const docData = docs.map(doc => {
		const data = doc.data() as PokemonLikedType
		return { ...data, idDocument: doc.id }
	})
	return docData.sort((pokemonA, pokemonB) => {
		return (pokemonA.id > pokemonB.id ? 1 : -1)
	})
}

export const addIdPokemon: (idPokemon:string) => Promise<PokemonLikedType> = async (idPokemon) => {
	const doc = await idPokemonLiked().add({id : idPokemon})
	const returnAdd:PokemonLikedType = {id: idPokemon, idDocument: doc.id}
	return { ...returnAdd}
}

export const deleteIdPokemon: (idDocument:string) => Promise<void> = async (idDocument) => {
	await idPokemonLiked().doc(idDocument).delete()
}
