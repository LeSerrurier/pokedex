import React from 'react'
import { StyleSheet, FlatList, Text, TouchableOpacity, View, Image } from 'react-native';
import PokemonType from "../type/type";
import PokemonComponentType from "./PokemonComponentType";

type Props = {
	pokemon: PokemonType;
}

const PokemonModel: React.FC<Props> = ({pokemon}) => {

	return (
        <View style={styles.container}>
            <Text>{pokemon.idDocument}</Text>
            <View style={styles.imageContainer}> 
                <Image style={styles.image} source={{uri:`${pokemon.img}`}} />
            </View>
            <View style={styles.infoContainer}>
                <View style={styles.titleContainer}>
                    <Text style={[styles.titleStyle, styles.id]}>{pokemon.id}</Text> 
                    <Text style={[styles.titleStyle, styles.name]}>{pokemon.name}</Text>
                </View>
                <FlatList
                    style={styles.types}
                    data={pokemon.type}
                    renderItem={({item}) => <PokemonComponentType type={item} />}
                    keyExtractor={(item)=> item + ""}
                />
            </View>
        </View>
    )
} 

const styles = StyleSheet.create({
    container:{
        flex: 1,
        borderWidth: 1,
        flexDirection: 'row',
        alignContent: "flex-start"
    },
    imageContainer: {
        width: 120,
        height: 120,
    },
    image: {
        width: '100%',
        height: '100%',
        resizeMode: "contain",
        justifyContent: "center",
    },
    infoContainer: {
        marginLeft: 20,
    },
    types: {
        flexDirection: 'column',
        width: 100
    },
    titleContainer: {
        flexDirection: 'row',
        marginBottom: 5
    },
    titleStyle: {
        fontSize: 23,
        fontWeight: "bold",
        letterSpacing: 1
    },
    id: {
        color: "grey",
        marginRight: 10
    },
    name: {
        color: "black",
        textTransform: 'uppercase'
    }
})  

export default PokemonModel
