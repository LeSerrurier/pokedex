import React from 'react'
import { StyleSheet, Text, View } from 'react-native';

type Props = {
	type: String;
}

const PokemonComponentType: React.FC<Props> = ({type}) => {

    const styleType = () => {
       switch(type){
            case "Normal " :
                return {backgroundColor: "#A8A77A"}
            case "Fire" :
                return {backgroundColor: "#EE8130"}
            case "Water" :
                return {backgroundColor: "#6390F0"}
            case "Electric" :
                return {backgroundColor: "#F7D02C"}
            case "Grass" :
                return {backgroundColor: "#7AC74C"}
            case "Ice" :
                return {backgroundColor: "#96D9D6"}
            case "Fighting" :
                return {backgroundColor: "#C22E28"}
            case "Poison" :
                return {backgroundColor: "#A33EA1"}
            case "Ground" :
                return {backgroundColor: "#E2BF65"}
            case "Flying" :
                return {backgroundColor: "#A98FF3"}
            case "Psychic" :
                return {backgroundColor: "#F95587"}
            case "Bug" :
                return {backgroundColor: "#A6B91A"}
            case "Rock" :
                return {backgroundColor: "#B6A136"}
            case "Ghost" :
                return {backgroundColor: "#735797"}
            case "Dragon" :
                return {backgroundColor: "#6F35FC"}
            case "Dark" :
                return {backgroundColor: "#705746"}
            case "Steel" :
                return {backgroundColor: "#B7B7CE"}
            case "Fairy" :
                return {backgroundColor: "#D685AD"}
            default :
                return {backgroundColor: "black"}
       }
    }

	return (
        <View style={[styles.typeContainer, styleType()]}>
            <Text style={styles.type}>{type}</Text>
        </View>
    )
} 

const styles = StyleSheet.create({
    typeContainer: {
        padding: 5,
        marginTop: 5,
        borderRadius: 20
    },
    type: {
        color: "white",
        fontSize: 15,
        textAlign: "center",
        textTransform: "uppercase",
        fontWeight: "bold"
    }
})  

export default PokemonComponentType
