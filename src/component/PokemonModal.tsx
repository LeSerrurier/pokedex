import React from 'react'
import { Text, View, StyleSheet, Image, FlatList } from 'react-native'
import PokemonType from "../type/type"
import PokemonComponentType from "../component/PokemonComponentType"

type Props = {
	pokemon: PokemonType
}
const PokemonModal: React.FC<Props> = ({ pokemon }) => {
	
	const caculatePower = (pokemon:PokemonType) => {
		const stats = pokemon.stats
		return (parseInt(stats.attack) + parseInt(stats.hp) + parseInt(stats.defense) + parseInt(stats.spattack) + parseInt(stats.spdefense) +parseInt(stats.speed))
	}

	return (
		<View style={styles.centeredView}>
			<View style={styles.modalView}>
				<View style={styles.modalContainerImage}>
					<View style={styles.imageContainerModal}> 
						<Image style={styles.imageModal} source={{uri:`${pokemon.img}`}} />
					</View>
				</View>

				<View style={styles.statsContainer}>
					<View style={styles.oneStatContainer}>
						<Text style={styles.titleModal}>ID : </Text>
						<Text style={styles.textModal}>#{pokemon.id}</Text>
					</View>
					<View style={styles.oneStatContainer}>
						<Text style={styles.titleModal}>Name : </Text>
						<Text style={styles.textModal}>{pokemon.name}</Text>
					</View>
					<View style={styles.oneStatContainer}>
						<FlatList
							horizontal={true}
							data={pokemon.type}
							renderItem={({item}) => <View style={{marginRight:15, width:100}}><PokemonComponentType type={item} /></View>}
							keyExtractor={(item)=> item + pokemon.id + ""}
						/>
					</View>
					<Text style={styles.titleCombatStatistic}>Combat statistic</Text>
					<View style={styles.statsCombat}>
						<Text style={styles.titleModal}>Power : </Text>
						<Text style={styles.textModal}>{caculatePower(pokemon)}</Text>
					</View>
					<View style={styles.statsCombat}>
						<Text style={styles.titleModal}>HP : </Text>
						<Text style={styles.textModal}>{pokemon.stats.hp}</Text>
					</View>
					<View style={styles.statsCombat}>
						<Text style={styles.titleModal}>Attack : </Text>
						<Text style={styles.textModal}>{pokemon.stats.attack}</Text>
					</View>
					<View style={styles.statsCombat}>
						<Text style={styles.titleModal}>Defense : </Text>
						<Text style={styles.textModal}>{pokemon.stats.defense}</Text>
					</View>
					<View style={styles.statsCombat}>
						<Text style={styles.titleModal}>SpeAttack : </Text>
						<Text style={styles.textModal}>{pokemon.stats.spattack}</Text>
					</View>
					<View style={styles.statsCombat}>
						<Text style={styles.titleModal}>SpeDefense : </Text>
						<Text style={styles.textModal}>{pokemon.stats.spdefense}</Text>
					</View>
					<View style={styles.statsCombat}>
						<Text style={styles.titleModal}>Speed : </Text>
						<Text style={styles.textModal}>{pokemon.stats.speed}</Text>
					</View>
				</View>
			</View>
		</View>
	  );
};

const styles = StyleSheet.create({
	centeredView: {
		flex: 1,
		alignItems: "center",
		marginTop: 20,
		marginBottom: 20
	},
	modalView: {
		width: 340,
		backgroundColor: "#d1cfcf",
		borderRadius: 20,
		padding: 25,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5,
	},
	buttonModal: {
		backgroundColor: "#2196F3",
		borderRadius: 20,
		padding: 10,
		elevation: 2,
		marginTop: 22
	},
	textButtonModal: {
		color: "white",
		fontWeight: "bold",
		textAlign: "center"
	},
	modalContainerImage: {
		flexDirection: "row",
		justifyContent: 'center',
	},
	imageContainerModal: {
		width: 250,
		height: 250,
		borderWidth: 3,
		backgroundColor: "white",
	},
	imageModal: {
		width: '100%',
		height: '100%',
		resizeMode: "contain",
		justifyContent: "center",
	},
	statsContainer: {
		flexDirection: "column",
		marginTop: 10
	},
	oneStatContainer: {
		flexDirection: "row",
		alignItems: 'center',
		marginBottom: 4
	},
	titleModal: {
		fontSize: 25,
		fontWeight: "bold",
	},
	textModal: {
		fontSize: 23,
	},
	titleCombatStatistic: {
		fontSize:26,marginTop:15, 
		marginBottom:10, 
		textAlign:"center",
		fontWeight:"bold"
	},
	statsCombat: {
		flexDirection:"row", 
		justifyContent:"space-between",
		borderBottomWidth: 1,
		marginBottom: 8
	}
  });

export default PokemonModal