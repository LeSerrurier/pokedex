type PokemonType = {
    idDocument?: string,
    id: string,
    name: string,
    img: string,
    type: string[],
    stats: PokemonStatType
}

export type PokemonStatType = {
    hp: string,
    attack: string,
    defense: string,
    spattack: string,
    spdefense: string,
    speed: string
}

export type PokemonLikedType = {
    idDocument: string,
    id: string
}

export default PokemonType
