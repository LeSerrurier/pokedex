import React, { useState } from 'react'
import { Text, TextInput, StyleSheet, Button, Alert, View, SafeAreaView, Image } from 'react-native'
import { useAuth } from '../context/AuthContext'

const AuthScreen = () => {
	const auth = useAuth()
	const [isRegister, setIsRegister] = useState(false)
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	const onPressAuth = async () => {
		if (!email || !password) {
			Alert.alert("Please fill in the fields")
			return
		}

		if (isRegister) {
			try {
				await auth.register(email, password)
				Alert.alert("Account was created")
				setEmail('')
				setPassword('')
			} catch (e) {
				Alert.alert(e)
			}
		}

		if (!isRegister) {
			try {
				await auth.signIn(email, password)
			} catch (e) {
				if (e.code === "auth/user-not-found") {
					Alert.alert('Ce compte n\'existe pas')
				}
				else {
					Alert.alert(e.message)
					console.warn(e.code)
				}
			}
		}
	}

	const toggleRegister = () => {
		setIsRegister(!isRegister)
	}

	return	<SafeAreaView style={styles.screen}>
		<View style={styles.containerLogo}>
			<Image style={styles.logo} source={{uri:'https://pokedex.juliettebois.fr/images/logo.png'}}/>
			<Text style={styles.title}>Pokédex</Text>
		</View>
		<Text style={styles.label}>
			Your mail
		</Text>
		<TextInput value={email} onChangeText={setEmail} style={styles.input} />
		<Text style={styles.label}>
			Your password
		</Text>
		<TextInput value={password} onChangeText={setPassword} style={styles.input} secureTextEntry={true} />
		<View style={styles.logInButton}>
			<Button title={isRegister ? 'Register' : 'Log In'} onPress={onPressAuth} />
		</View>

		<Text style={styles.label}>
			{isRegister ? 'Already have an account ? ' : 'You don\'t have an account?'}
		</Text>
		<Button title={isRegister ? 'Log in here ' : 'Register here'} onPress={toggleRegister} />

	</SafeAreaView>
}


const styles = StyleSheet.create({
	screen: {
		padding: 20
	},
	containerLogo: {
		height: 100,
		marginBottom: 60,
	},
	logo: {
		width: '100%',
        height: '100%',
        resizeMode: "contain",
        justifyContent: "center",
	},
	title: {
		fontSize: 40,
		textAlign: "center"
	},
	label: {
		fontSize: 20,
		marginBottom: 5
	},
	input: {
		width: '100%',
		fontSize: 18,
		borderWidth: 1,
		marginBottom: 16,
		height: 45,
	},
	logInButton: {
		marginBottom: 40
	}
});


export default AuthScreen