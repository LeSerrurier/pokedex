import React, { useEffect, useState } from 'react';
import { View, SafeAreaView, StyleSheet, TouchableOpacity, Text, FlatList, Image, TextInput, ScrollView, Modal } from 'react-native';
import { useAuth } from '../context/AuthContext';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import * as PokemonService from "../service/pokemon";
import database from '@react-native-firebase/database';
import PokemonType from "../type/type"
import PokemonComponentType from '../component/PokemonComponentType';
import {Picker} from '@react-native-picker/picker';
import PokemonModal from '../component/PokemonModal';

const pokemonDefault:PokemonType = {
	id: "000",
	name: "Default",
	img: "",
	type: ["None"],
	stats: {hp: "0", 
			attack: "0",
			spattack: "0",
			defense: "0",
			spdefense: "0",
			speed: "0"}
}
const pokemonsDefault:PokemonType[] = []
var pokemonsList:PokemonType[] = []

const HomeScreen = () => {

	/**
	 * allPokemons / pokemonLiked : tableaux qui stockent tous les pokémons présents dans le back-end pour ne pas avoir à le solliciter à chaque fois que c'est nécessaire
	 * pokemonDisplay : c'est la liste des pokémons affichés à l'écran
	 * stateDisplay : permet d'afficher le sous menu dans lequel souhaite aller l'utilsateur - "Pokedex" || "Liked"
	 * stateModal : permet d'afficher le modal du pokémon que souhaite l'utilisateur
	 * pokemonModal : pokémon affiché sur la modal
	 * 
	 * newPokemonFilter : valeur de la recherche de l'utilisateur
	 * pickerFilter : valeur du sélecteur - "name" || "power" || "type"
	 * statusSortByName/ByStats/ByType : permet de trier les listes des pokémons selon le(s) critère(s) que souhaite l'utilisateur
	 */

	const [allPokemons, setAllPokemons] = useState<PokemonType[]>(pokemonsDefault)
	const [pokemonLiked, setPokemonLiked] = useState<PokemonType[]>(pokemonsDefault)
	const [pokemonsDisplay, setPokemonsDisplay] = useState<PokemonType[]>(pokemonsDefault)
	const [stateDisplay, setStateDisplay] = useState<string>("Pokedex")
	const [stateModal, setStateModal] = useState<boolean>(false);
	const [pokemonModal, setPokemonModal] = useState<PokemonType>(pokemonDefault)

	const [newPokemonFilter, setNewPokemonFilter] = useState<string>("")
	const [pickerFilter, setPickerFilter] = useState<string>("name")
	const [statusSortByName, setStatusSortByName] = useState<boolean>(false)
	const [statusSortByStats, setStatusSortByStats] = useState<boolean>(false)
	const [statusSortByType, setStatusSortByType] = useState<boolean>(false)

	useEffect(() => {
		if(pokemonLiked.length == 0)
			initPokemonData()
	}, [])

	const initPokemonData = async() => {
		database()
		.ref()
		.orderByChild('id')
		.once('value')
		.then(snapshot => {
			const pokemons:PokemonType[] = snapshot.val()
			PokemonService.getIdPokemon().then(idPokemonsLiked => {
				idPokemonsLiked.forEach(idPokemonLiked => {
					pokemons.forEach(_pokemon =>{
						if(idPokemonLiked.id == _pokemon.id){
							_pokemon.idDocument = idPokemonLiked.idDocument
							pokemonLiked.push(_pokemon)
						}
					})
				})
				setPokemonLiked([...pokemonLiked])
				setAllPokemons([...pokemons])
			})
			setPokemonsDisplay([...pokemons])
			pokemonsList = pokemons
		});
	}

	const auth = useAuth()

	const onPressLogOut = () => {
		auth.signOut()
	}

	const showPokedex = () => {
		setPokemonsDisplay([...allPokemons])
		setStateDisplay("Pokedex")
	}

	const showLiked = () => {
		setPokemonsDisplay([...pokemonLiked])
		setStateDisplay("Liked")
	}

	const styleMenu = (type:String) => {
		if(stateDisplay == "Pokedex") {
			return (type == "Pokedex" ? {borderBottomWidth: 2} : {})
		} else if(stateDisplay == "Liked") {
			return (type == "Pokedex" ? {} : {borderBottomWidth: 2})
		}
	}

	const styleSortButton = (sortType:string) => {
		if( (statusSortByName && sortType == "Name") || (statusSortByStats && sortType == "Stats") || (statusSortByType && sortType == "Type"))
			return {backgroundColor: "#07b519"}
	}

	const styleLikeButton = (pokemon:PokemonType) => {
		return (pokemon.idDocument == undefined ? <AntDesign name="like2" size={50} color="grey" /> : <AntDesign name="like1" size={50} color="grey" />)
	}

	const likePokemon = async (pokemon:PokemonType) => {
		var newPokemonLikedList = pokemonLiked
		var newAllPokemonList = allPokemons
		if(pokemon.idDocument != undefined) { 
			const idDocument = pokemon.idDocument
			var index = newPokemonLikedList.findIndex(_pokemon => {
				return pokemon.idDocument ===  _pokemon.idDocument
			})
			newPokemonLikedList.splice(index,1)
			newAllPokemonList.forEach(_pokemon => {
				if(pokemon.idDocument ==  _pokemon.idDocument)
					_pokemon.idDocument = undefined
			})
			updatePokemonList(newPokemonLikedList, newAllPokemonList)
			PokemonService.deleteIdPokemon(idDocument)
		} else {
			PokemonService.addIdPokemon(pokemon.id).then(idPokemonliked => {
				newAllPokemonList.forEach(pokemon => {
					if(pokemon.id == idPokemonliked.id){
						pokemon.idDocument = idPokemonliked.idDocument
						newPokemonLikedList.push(pokemon)
					}
				})
				if(!statusSortByName && !statusSortByStats && !statusSortByType)
					newPokemonLikedList = newPokemonLikedList.sort((pokemonA, pokemonB) => (parseInt(pokemonA.id) > parseInt(pokemonB.id) ? 1 : -1))
				else 
					newPokemonLikedList = sortPokemon(newPokemonLikedList)
				updatePokemonList(newPokemonLikedList, newAllPokemonList)
			})
		}
	}

	const updatePokemonList = (newPokemonLikedList:PokemonType[], newAllPokemonList:PokemonType[]) => {
		setPokemonLiked([...newPokemonLikedList])
		setAllPokemons([...newAllPokemonList])
		setPokemonsDisplay([...(stateDisplay == "Pokedex" ? allPokemons : pokemonLiked)])
	}

	const caculatePower = (pokemon:PokemonType) => {
		const stats = pokemon.stats
		return (parseInt(stats.attack) + parseInt(stats.hp) + parseInt(stats.defense) + parseInt(stats.spattack) + parseInt(stats.spdefense) +parseInt(stats.speed))
	}

	const sortPokemon = (listPokemon:PokemonType[]) => {
		var sortListPokemon
		if(statusSortByName) {
			sortListPokemon = listPokemon.sort((pokemonA, pokemonB) => {
				return (pokemonA.name > pokemonB.name ? 1 : -1)
			})
		} else if(statusSortByType) {
			sortListPokemon = listPokemon.sort((pokemonA, pokemonB) => {
				if		(pokemonA.type.length > pokemonB.type.length) return -1
				else if (pokemonA.type.length < pokemonB.type.length) return 1
				else {
						pokemonA.type.sort()
						pokemonB.type.sort()
						var typesA = ""
						var typesB = ""

						pokemonA.type.forEach(typeA => {
							typesA += typeA
						})
						pokemonA.type.forEach(typeB => {
							typesB += typeB
						})
						return (typesA >= typesB ? -1 : 1)
					}
				}
			)
		} else if(statusSortByStats) {
			sortListPokemon = listPokemon.sort((pokemonA, pokemonB) => {
				const powerA = caculatePower(pokemonA)
				const powerB = caculatePower(pokemonB)
				return (powerA >= powerB ? -1 : 1)
			})
		} else {
			sortListPokemon = listPokemon.sort((pokemonA, pokemonB) => {
				return (pokemonA.id > pokemonB.id ? 1 : -1)
			})
		}
		return sortListPokemon
	}

	useEffect(() => {
		const sortAllPokemon = sortPokemon(allPokemons)
		setAllPokemons([...sortAllPokemon])

		const sortPokemonLiked = sortPokemon(pokemonLiked)
		setPokemonLiked([...sortPokemonLiked])

		setPokemonsDisplay([...(stateDisplay == "Pokedex" ? sortAllPokemon : sortPokemonLiked)])
	
	}, [statusSortByName, statusSortByStats, statusSortByType])

	const filterAnyPokemonList = (filterPokemonListToFilter:PokemonType[], text:string) => {
		filterPokemonListToFilter = filterPokemonListToFilter.filter(pokemon => {
			switch(pickerFilter){
				case "name":
					return pokemon.name.toLowerCase().includes(text.toLowerCase())
				case "id" :
					return pokemon.id.includes(text)
				case "type" :
					var allType = ""
					pokemon.type.map(_type => allType += _type)
					return allType.toLowerCase().includes(text.toLowerCase())
				default :
					return pokemon.name.toLowerCase().includes(text.toLowerCase())
			}
		})
		return filterPokemonListToFilter
	}

	const filterPokemon = (text:string) => {
		var filterAllPokemon = filterAnyPokemonList(pokemonsList, text)
		setAllPokemons([...filterAllPokemon])
		
		var filterPokemonLiked:PokemonType[] = []
		PokemonService.getIdPokemon().then(idPokemonsLiked => {
			idPokemonsLiked.forEach(idPokemonLiked => {
				pokemonsList.forEach(_pokemon =>{
					if(idPokemonLiked.id == _pokemon.id){
						filterPokemonLiked.push(_pokemon)
					}
				})
			})
			filterPokemonLiked = filterAnyPokemonList(filterPokemonLiked, text)
			setPokemonLiked([...filterPokemonLiked])
			setPokemonsDisplay([...(stateDisplay == "Pokedex" ? filterAllPokemon : filterPokemonLiked)])
		})
	}

	return (
		<SafeAreaView style={styles.screen}>

			<View style={{ flexDirection: "row", justifyContent: 'space-between'}}>
				<TouchableOpacity onPress={() => showPokedex()}> 
					<Text style={[styles.title, styleMenu("Pokedex")]}>POKEDEX</Text> 
				</TouchableOpacity>
				<TouchableOpacity onPress={() => showLiked()}> 
					<Text style={[styles.title, styleMenu("Liked")]}>LIKED</Text> 
				</TouchableOpacity>
				<TouchableOpacity onPress={() =>onPressLogOut()}> 
					<FontAwesome name="sign-out" size={38} color="#000" />
				</TouchableOpacity>
			</View>
			
			<View style={styles.filterAndSort}>
				<View style={styles.filterContainer}>
					<TextInput 
						style={styles.inputSearch}
						value={newPokemonFilter}
						onChangeText={(text) => {
							setNewPokemonFilter(text);
							filterPokemon(text)   
						}}
						placeholder="Search Pokemon"
					/>
					<View style={styles.pickerContainer}>
						<Picker
							selectedValue={pickerFilter}
							style={styles.picker}
							onValueChange={(itemValue) => {
								setPickerFilter(itemValue.toString())
							}}>
							<Picker.Item label="Name" value="name" />
							<Picker.Item label="Type" value="type" />
							<Picker.Item label="ID" value="id" />
						</Picker>
					</View>
				</View>

				<View style={styles.sort}>
					<Text style={styles.titleSort}>Sort by :</Text>
					<TouchableOpacity style={[styles.buttonSort, styleSortButton("Name")]} onPress={() => setStatusSortByName(!statusSortByName)}>
						<Text style={styles.textSort}>NAME</Text>
					</TouchableOpacity>
					<TouchableOpacity style={[styles.buttonSort, styleSortButton("Stats")]} onPress={() => setStatusSortByStats(!statusSortByStats)}>
						<Text style={styles.textSort}>STATS</Text>
					</TouchableOpacity>
					<TouchableOpacity style={[styles.buttonSort, styleSortButton("Type")]} onPress={() => setStatusSortByType(!statusSortByType)}>
						<Text style={styles.textSort}>TYPE</Text>
					</TouchableOpacity>
				</View>
			</View>

			<Modal animationType="slide" transparent={true}	visible={stateModal}>		
				<ScrollView>
					<TouchableOpacity onPress={() => {setStateModal(!stateModal)}}>	
						<PokemonModal pokemon={pokemonModal}/>
					</TouchableOpacity>
				</ScrollView>
			</Modal>

			<FlatList
				data={pokemonsDisplay}
				initialNumToRender={10}
				maxToRenderPerBatch={25}
				updateCellsBatchingPeriod={20}
				removeClippedSubviews={true}
				contentContainerStyle={{ paddingBottom: 150 }}
				renderItem={({item}) => (
					<TouchableOpacity onPress={() => {setPokemonModal(item), setStateModal(!stateModal)}}>
						<View style={styles.pokemonContainer}>
							<View style={styles.imageContainer}> 
								<Image style={styles.image} source={{uri:`${item.img}`}} />
							</View>
							<View style={styles.infoContainer}>
								<View style={styles.titleContainer}>
									<Text style={[styles.titleStyle, styles.id]}>{item.id}</Text> 
									<Text style={[styles.titleStyle, styles.name]}>{item.name}</Text>
								</View>
								<View style={styles.typeAndLikeContainer}>
									<FlatList
										style={styles.types}
										data={item.type}
										renderItem={({item}) => <PokemonComponentType type={item} />}
										keyExtractor={(item)=> item + ""}
									/>
									<TouchableOpacity onPress={() => likePokemon(item)}>
										<View style={styles.likeContainer}>
											{styleLikeButton(item)}
										</View>	
									</TouchableOpacity>
								</View>
							</View>
						</View>
					</TouchableOpacity>)}
				keyExtractor={(item)=> item.id + ""}
			/>

		</SafeAreaView>)
}


const styles = StyleSheet.create({

	screen: {
		padding: 15,
	},
	title: {
		fontSize: 25,
		fontWeight: "700",
		marginBottom: 15,
		color: "black",
		letterSpacing: 5,
	},
	labelLogOut: {
		letterSpacing: 0
	},
	filterAndSort: {
		marginBottom: 20
	},
	filterContainer : {
		flexDirection: "row",
	},
	pickerContainer: {
		borderWidth: 1,
		borderColor: 'grey',
		width: 110,
		borderTopRightRadius: 15,
		borderBottomRightRadius: 15
	},
	picker: {
		width: "100%"
	},
	inputSearch: {
		fontSize: 20,
        borderWidth: 1,
        borderColor: 'grey',
        borderTopLeftRadius: 25,
        borderBottomLeftRadius: 25,
        paddingLeft: 15,
		paddingRight: 15,
		width: 252
	},
	sort: {
		marginTop: 10,
		flexDirection: "row",
		alignItems: "center"
	},
	titleSort: {
		fontSize: 20,
	},
	buttonSort: {
		marginLeft: 10,
		width: 80,
		backgroundColor: "#3a7ce8",
		paddingTop: 6,
		paddingBottom: 7,
	},
	textSort: {
		textAlign: "center",
		color: "white",
		fontWeight: "bold",
		fontSize: 16
	},
	pokemonContainer:{
        flex: 1,
        flexDirection: 'row',
		alignContent: "flex-start",
		marginBottom: 25,
		backgroundColor: "white",
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.22,
		shadowRadius: 2.22,
		elevation: 3,
    },
    imageContainer: {
        width: 110,
        height: 110,
    },
    image: {
        width: '100%',
        height: '100%',
        resizeMode: "contain",
        justifyContent: "center",
    },
    infoContainer: {
		marginLeft: 20,
		flexDirection: "column",
		width: 210
    },
    types: {
        flexDirection: 'column',
        width: 100
    },
    titleContainer: {
        flexDirection: 'row',
		marginBottom: 5,
    },
    titleStyle: {
        fontSize: 23,
        fontWeight: "bold",
        letterSpacing: 1
    },
    id: {
        color: "grey",
        marginRight: 10
    },
    name: {
        color: "black",
        textTransform: 'uppercase'
	},
	typeAndLikeContainer: {
		flexDirection: "row",
		justifyContent: "space-around",
	},
	likeContainer: {
		borderRadius: 100,
		borderWidth: 4,
		borderColor: "grey",
		padding: 6
	},
});


export default HomeScreen