import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AuthScreen from '../screen/AuthScreen';
import HomeScreen from '../screen/HomeScreen';
import { useAuth } from '../context/AuthContext';
import { createStackNavigator } from '@react-navigation/stack';

export type MainStackParamList = {
	AuthScreen: undefined
	HomeScreen: undefined
}

const Stack = createStackNavigator<MainStackParamList>();

export default function MainRouter() {

	const auth = useAuth()

	const renderRoutes = () => {

		if (!auth.isSignedIn) 
			return <Stack.Screen name="AuthScreen" component={AuthScreen} options={{headerShown: false }}/>
		
		if (auth.isSignedIn)
			return <>
				<Stack.Screen name="HomeScreen" component={HomeScreen} options={{headerShown: false }}/>
			</>
	}

	return (
		<NavigationContainer>
			<Stack.Navigator>
				{renderRoutes()}
			</Stack.Navigator>
		</NavigationContainer>
	);
}
